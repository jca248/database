README
People List

This webpage is to have a database of people and their attributes such as weight, height, eye color, favourite/preferred font, and happiness scale.
I have borrowed some functions from scaffold and edited it to fit my desired outcome.
In the display page, you can see a visual of their inputs such as their desired font and the jumps represent their happiness scale.